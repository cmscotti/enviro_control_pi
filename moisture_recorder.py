import RPi.GPIO as GPIO
import datetime
GPIO.setmode(GPIO.BOARD)
GPIO.setup(7,GPIO.IN)

def get_moisture_reading():
    now = datetime.datetime.now()
    f =  open('/home/pi/enviro_control_pi/moisture.txt','a')
    reading = GPIO.input
    if reading:
        result = 'TRUE'
    else:
        result = 'FALSE'
    time_stamp = now.strftime("%Y-%m-%d %H:%M")
    f.write('\n Moisture Reading for ' + time_stamp + ':' + result)

get_moisture_reading()

GPIO.cleanup()
